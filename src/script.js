

//                                    WORLD
//                                    -----

const world = {"turn":undefined,"turn_index":1,"break":false,"target_dom":undefined,"target":undefined,"card_dom":undefined,"card":undefined,"last_action":"no action"};


//                                 Locations
//                                 ---------

world.locations = {};


const Location = function(options){

  this.name;
  this.paths = [];
  this.content = [];

  
  for( i in options ) this[i] = options[i];


  world.locations[this.name] = this;
};


//                                  Entities
//                                  --------

world.entities = {};


const Entity = function(options){
  
  this.name;
  this.pv;
  this.pa = 0;
  this.pd = 0;
  this.location = undefined;
  this.targeted = undefined;
  this.actions = [];
  this.hand = [];
  this.discard = [];
  this.permanents = [];
  this.choices = undefined;
  this.status = {};
  this.turn = "false";
  

  for( i in options ) this[i] = options[i];
  

  world.entities[this.name] = this;
};


Entity.prototype.move = function(location_name){
  
  if ( this.location != undefined ) {
  
    let array = world.locations[this.location].content;
    let index = array.indexOf(this);
    if ( index > -1 ) array.splice(index,1);
  };


  this.location = location_name;
  world.locations[location_name].content.push(this);
  console.log(this.name,"move to",location_name);
};


Entity.prototype.act = function(action_name,entity_name) {
 
  world.entities[entity_name].targeted = world.actions[action_name];
  console.log("targeted");
  console.log(this.name,"act",action_name);
 

  if( this.hand.indexOf(action_name) < 0 ){
    return console.log("Action not in hand");
  }; 
 

  if ( world.entities[entity_name].location != this.location ) {
    return console.log("Entity not in range");
  };


  if ( world.turn != this ) return console.log("Not",this.name,"turn");
  if ( this.pa < world.actions[action_name].cost ) return console.log("Not enough PA");


  this.pa -= world.actions[action_name].cost;
  let index = this.hand.indexOf(action_name);
  this.discard.push(this.hand.splice(index,1)[0]); 
  world.last_action = action_name;


  for ( var i = 0 ; i < world.actions[action_name].effects.length; i++ ){
   
     if( world.break ){

      world.break = false;
      console.log("retry");
      break;
    };
    //TODO BUG WITH DEAD ENTITIES ???
    console.log(world.actions[action_name].effects[i][0],action_name);
    console.log(
      world.effects[
        world.actions[action_name].effects[i][0]].result(
          world.entities[entity_name],world.actions[action_name].effects[i][1]
        )
    );
  };


  update_world();
  update_display();
};


Entity.prototype.end_turn = function(){
  if ( !world.player.entity ) return 
  if ( world.turn != this ) return console.log("Not",this.name,"turn");
  this.turn = world.turn_index;
  this.pa = 0;
  world.turn = undefined;
  console.log( "End of turn :",this.name );


  for ( var i = 0 ; i < world.locations[this.location].content.length ; i++ ){
    if( world.locations[this.location].content[i].turn == "false" ) {

      world.turn = world.locations[this.location].content[i];
      world.turn.pd = 0;
  
      world.turn.status = {};
      for ( var i = 0 ; i < world.turn.permanents.length ; i++ ) {
        for ( var j = 0 ; j < world.actions[world.turn.permanents[i]].effects.length; j++ ){
          if( world.break ){
            world.break = false;
            console.log("retry");
            break;
          };
          if ( world.actions[world.turn.permanents[i]].effects[j][0] == "permanent" ) continue;
          world.effects[
            world.actions[world.turn.permanents[i]].effects[j][0]].result(
              world.entities[world.turn.name],world.actions[world.turn.permanents[i]].effects[j][1]
            )
        };
      };

      world.turn.draw(1);
      world.turn_index ++;
      world.last_action = "none";
      console.log("Its",world.turn.name,"turn !");


      if ( world.turn != world.player.entity ) {
        if ( world.turn.pv ) {
          setTimeout(function(){
            fighting(world.turn);
          },500);
        } else {
          fighting(world.turn);
        };
      };


      update_display();
      return
    };
  };


  let next = undefined


  for ( var i = 0 ; i < world.locations[this.location].content.length ; i++ ){

    if ( world.locations[this.location].content[i].turn == 1 ){

      next = world.locations[this.location].content[i];
    };


    world.locations[this.location].content[i].turn = "false";
  };
  

  world.turn = next;
  world.turn_index = 1;
  world.turn.pd = 0;
  
  world.turn.status = {};
  for ( var i = 0 ; i < world.turn.permanents.length ; i++ ) {
    for ( var j = 0 ; j < world.actions[world.turn.permanents[i]].effects.length; j++ ){
      if( world.break ){
        world.break = false;
        console.log("retry");
        break;
      };
      if ( world.actions[world.turn.permanents[i]].effects[j][0] == "permanent" ) continue;
      world.effects[
        world.actions[world.turn.permanents[i]].effects[j][0]].result(
          world.entities[world.turn.name],world.actions[world.turn.permanents[i]].effects[j][1]
        )
    };
  };

  world.turn.draw(1);
  world.last_action = "none";
  console.log("Its",world.turn.name,"turn !");


  if ( world.turn != world.player.entity ) {

    fighting(world.turn);
  };
  

  update_display();
  return
};


Entity.prototype.draw = function(n){

  for ( var i = 0 ; i < n ; i++ ){

    let action = this.actions.pop();


    if ( action ){

      this.hand.push(action);
    } else {

      if ( this.discard.length ) {

        this.shuffle();
        this.draw(n);
      };
    };
  };
};

Entity.prototype.shuffle = function(){

  let j, x, i;
  let actions = this.discard.slice();
  this.discard = [];
  
  //if ( world.player.entity == this ) {
    for (i = actions.length - 1; i > 0; i--) {

        j = Math.floor(Math.random() * (i + 1));
        x = actions[i];
        actions[i] = actions[j];
        actions[j] = x;
    };
  //};


  this.actions = actions;
};


//                                   Player
//                                   ------

Player = function(options){

  this.entity;
  

  for( i in options ) this[i] = options[i];
};


Player.prototype.look = function( ){

  for ( var i = 0 ; i < world.locations[this.entity.location].paths.length ; i++ ) {

    console.log("path :",world.locations[this.entity.location].paths[i]);
  };
  

  for ( var i = 0 ; i < world.locations[this.entity.location].content.length ; i++ ) {

    console.log("entity :",world.locations[this.entity.location].content[i].name);
  };


  for ( var i = 0 ; i < this.entity.hand.length ; i++ ) {

    console.log("action :",this.entity.hand[i]);
  };
};


//                                  Effects
//                                  -------

world.effects = {};


const Effect = function(options){

  this.name;
  this.result;
  
  
  for( i in options ) this[i] = options[i];


  world.effects[this.name] = this;
};


//                                  Actions
//                                  -------

world.actions = {};


const Action = function(options){

  this.name;
  this.cost;
  this.effects = [];


  for( i in options ) this[i] = options[i];


  world.actions[this.name] = this;
};


//                                 Behaviour
//                                 ---------

function fighting(entity){

  if ( !world.player.entity.pv ) return console.log("game over");
  if ( !entity.pv ) return entity.end_turn();


  if ( entity.choices == undefined ) {
    entity.choices = [];
    let mouvements = [];
    let max_mouvements = 0;
    let costs = []; 
    let final_cost = 0; 

    for ( var i = 0 ; i < entity.hand.length ; i++ ){
      let action = world.actions[entity.hand[i]]
      if( action.type == "mouvement" ) {
        for ( var j = 0 ; j < action.effects.length; j++ ){
          if( action.effects[j][0] == "temps" ) {
            mouvements.push(action.effects[j][1]);
            max_mouvements += action.effects[j][1];
          };
        };
      } else {
        costs.push(action.cost);
      };
    };
    
    console.log( "NEED", max_mouvements, entity.behaviour_need );
    if ( max_mouvements < entity.behaviour_need ) return fighting(entity);

    // mouvements += entity.pa;

    console.log( "mouvements",mouvements,"costs",costs,"max_mouvements",max_mouvements );
    
    for ( var i = 0 ; i < entity.hand.length ; i++ ){
      let action = world.actions[entity.hand[i]]
      if( action.type != "mouvement" ) {
        if ( action.cost < max_mouvements ) {
          entity.choices.push( action.name ) ;
          max_mouvements -= action.cost;
          final_cost += action.cost;
        };
      };
    };

    console.log( "max_mouvements", max_mouvements, entity.choices, "final_cost", final_cost );
    
    let choices = [];

    while ( entity.choices.length ) {
      let max_cost = 0;
      for ( var i = 0 ; i < entity.choices.length ; i++ ){
        if ( world.actions[entity.choices[i]].cost > max_cost ){
          max_cost = world.actions[entity.choices[i]].cost;
        };
      };
      for ( var i = 0 ; i < entity.choices.length ; i++ ){
        if ( world.actions[entity.choices[i]].cost == max_cost ){
          choices.push(entity.choices.splice(i,1)[0]); 
        };
      };
    };
    
    entity.choices = choices;
    
    for ( var i = 0 ; i < entity.hand.length ; i++ ){
      let action = world.actions[entity.hand[i]]
      if( action.type == "mouvement" ) {
        for ( var j = 0 ; j < action.effects.length; j++ ){
          if( action.effects[j][0] == "temps" && final_cost > 0 ) {
            final_cost -= action.effects[j][1];
            entity.choices.unshift( action.name );
          };
        };
      } ;
    };


    console.log(entity.choices);
    fighting(entity);

  } else {
    

    if ( entity.choices.length ){


      if ( world.actions[entity.choices[0]].effects[0][0] == "self" ){

        entity.act(entity.choices[0],entity.name);
        entity.choices.shift();
      } else {

        entity.act(entity.choices[0],world.player.entity.name);
        entity.choices.shift();
      };

      update_world();
      update_display();
  

      setTimeout(function(){
        fighting(entity);
      },500);
    } else {
      entity.choices = undefined;
      setTimeout(function(){
        entity.end_turn();
      },500);
    };
  };
};


//                                 INTERFACE
//                                 ---------

//                                   View 
//                                   ---- 

let view_dom = document.createElement("section");
view_dom.id = "view";
let view_title = document.createElement("h2");
view_title.id = "view_title";
let view_content = document.createElement("div");
view_content.id = "view_content";
view_dom.appendChild(view_title);
view_dom.appendChild(view_content);
document.body.appendChild(view_dom);

//                                   Turn 
//                                   ---- 

let turn_dom = document.createElement("section");
turn_dom.id = "turn";
let turn_title = document.createElement("p");
turn_title.id = "turn_title";
let turn_content = document.createElement("div");
turn_content.id = "turn_content";
let last_action = document.createElement("div");
last_action.id = "last_action";
turn_content.textContent = "pass turn";
turn_content.addEventListener("click",function(){
  world.player.entity.end_turn();
});
document.body.appendChild(turn_dom);


//                                   Actions
//                                   -------
/*
let actions_dom = document.createElement("section");
actions_dom.id = "actions";
let actions_title = document.createElement("h2");
actions_title.id = "actions_title";
actions_title.textContent = "Actions :";
let actions_content = document.createElement("div");
actions_content.id = "actions_content";
actions_dom.appendChild(actions_title);
actions_dom.appendChild(actions_content);
document.body.appendChild(actions_dom);

*/
//                                   Hand 
//                                   ---- 

let hand_dom = document.createElement("section");
hand_dom.id = "hand";
let hand_content = document.createElement("div");
hand_content.id = "hand_content";
hand_dom.appendChild(hand_content);
document.body.appendChild(hand_dom);


//                                   Update
//                                   ------

function player_input(){
  for ( var i = 0 ; i < world.actions[world.card].effects.length ; i++ ){
    console.log(world.actions[world.card]);
    let effect = world.actions[world.card].effects[i][0];
    if( effect == "self" ) {
      world.target = world.player.entity.name;
    };
  };

  if ( world.card != undefined && world.target != undefined ) {
    world.player.entity.act(world.card,world.target);
    world.card = undefined;
    world.target = undefined;
  };
};

function update_display(){

while(view_content.children.length) view_content.removeChild(view_content.children[0]);
while(hand_content.children.length) hand_content.removeChild(hand_content.children[0]);
while(turn_dom.children.length) turn_dom.removeChild(turn_dom.children[0]);


//                                    View

  view_dom.style.backgroundImage = "none";
  view_title.textContent = "";

  if( !world.player.entity ) {
    let game_over = document.createElement("h1");
    game_over.style.color = "#F00";
    game_over.style.fontSize = "8rem";
    game_over.textContent = "GAME OVER";
    view_content.appendChild(game_over);
    return
  }

  view_title.textContent = world.player.entity.location;
  view_dom.style.backgroundImage = "url(./images/entities/"+world.player.entity.location+".png)";


  for ( var i = 0 ; i < world.locations[world.player.entity.location].content.length ; i ++ ) {
    
    let entity = world.locations[world.player.entity.location].content[i];
    

    let content = document.createElement("div");
    content.setAttribute("entity",entity.name);
    content.classList.add("content");
    if ( entity == world.turn ){
      content.style.backgroundImage = 'url("images/yellow_transparent_circle.png")';
    }
    content.addEventListener("click",function(){
      if( world.target_dom != undefined ) {
        world.target_dom.style.backgroundColor = "transparent";
      }
      world.target_dom = this;
      this.style.backgroundColor = "#008";
      world.target = this.getAttribute("entity");
      player_input();
    });
    if ( entity.targeted != undefined ) {
      console.log("targeted_dom",entity.targeted.type);
      if ( entity.targeted.type == "mouvement" ){
        content.style.backgroundColor = "#ff0";
        console.log("yellow");
      };
      if ( entity.targeted.type == "defense" ){
        content.style.backgroundColor = "#00f";
      };
      if ( entity.targeted.type == "attack" ){
        content.style.backgroundColor = "#f00";
      };
      if ( entity.targeted.type == "objet" ){
        content.style.backgroundColor = "#0ff";
      };
      if ( entity.targeted.type == "equipement" ){
        content.style.backgroundColor = "#888";
      };
      setTimeout(function(){
        this.style.backgroundColor = "transparent";
        world.entities[this.getAttribute("entity")].targeted = undefined;
      }.bind(content),100);
    }


    if ( entity == world.player.entity ) {

      content.classList.add("player");
    } else {

      content.classList.add("not_player");
    };
    

    let content_name = document.createElement("h3");
    content_name.textContent = entity.name;
    

    let content_art = document.createElement("img");
    content_art.classList.add("art");
    content_art.src = entity.art;


    let content_attributes = document.createElement("div");
    content_attributes.classList.add("content_attributes");
    let content_pv = document.createElement("div");
    content_pv.classList.add("pv");
    content_pv.textContent = entity.pv;
    content_attributes.appendChild(content_pv);    
    let content_pd = document.createElement("div");
    content_pd.classList.add("pd");
    content_pd.textContent = +entity.pd;
    content_attributes.appendChild(content_pd);    
    let content_pa = document.createElement("div");
    content_pa.classList.add("pa");
    content_pa.textContent = entity.pa;
    content_attributes.appendChild(content_pa);
    var br = document.createElement("br");
    content_attributes.appendChild(br);
    for ( var j = 0 ; j < entity.permanents.length; j++ ){
      let equipement = document.createElement("div");
      equipement.classList.add("equipement");
      equipement.textContent = entity.permanents[j].charAt(0).toUpperCase() + entity.permanents[j].slice(1);
      content_attributes.appendChild(equipement);
    }; 
    var br = document.createElement("br");
    content_attributes.appendChild(br);
    for ( var j in entity.status ){
      let status = document.createElement("div");
      status.classList.add("status");
      status.textContent = j.charAt(0).toUpperCase() + j.slice(1) + " " + entity.status[j];
      content_attributes.appendChild(status);
    }; 


    content.appendChild(content_name);
    content.appendChild(content_attributes);
    content.appendChild(content_art);


    content.addEventListener( "click", function(){
      console.log("coucou");
    });


    view_content.appendChild(content); 
  };


//                                    Turn

  turn_title.textContent = world.turn.name;
  turn_dom.appendChild(turn_title);
  last_action.textContent = world.last_action;
  if ( world.actions[world.last_action] ) {
    if ( world.actions[world.last_action].type == "attack" ) {
      last_action.style.borderColor = "#800";
    } else if ( world.actions[world.last_action].type == "defense" ) {
      last_action.style.borderColor = "#008";
    } else if ( world.actions[world.last_action].type == "objet" ) {
      last_action.style.borderColor = "#088";
    } else if ( world.actions[world.last_action].type == "mouvement" ) {
      last_action.style.borderColor = "#880";
    };
  } else {
      last_action.style.borderColor = "#888";
  };
  turn_dom.appendChild(last_action);
  if( world.turn == world.player.entity ) turn_dom.appendChild(turn_content);


//                                    Hand


  for ( var i = 0 ; i < world.player.entity.hand.length ; i ++ ) {
    
    let card = world.player.entity.hand[i];

    
    let card_dom = document.createElement("div");
    card_dom.setAttribute("card",card);
    card_dom.classList.add("card");
    card_dom.classList.add(world.actions[card].type);
    card_dom.addEventListener("click",function(){
      if( world.card_dom != undefined ) {
        world.card_dom.style.backgroundColor = "transparent";
      }
      world.card_dom = this;
      this.style.backgroundColor = "#008";
      world.card = this.getAttribute("card");
      player_input();
    });

    
    let card_header = document.createElement("div");
    card_header.classList.add("card_header");


    let card_name = document.createElement("div");
    card_name.classList.add("card_name");
    card_name.textContent = card.charAt(0).toUpperCase() + card.slice(1);
    
    
    let card_cost = document.createElement("div");
    card_cost.classList.add("card_cost");
    card_cost.textContent = world.actions[card].cost;


    let card_descr = document.createElement("p");
    card_descr.classList.add("card_descr");
    card_descr.textContent = world.actions[card].descr;

/*
    card_dom_attributes.appendChild(card_dom_pv);    
    let card_dom_pd = document.createElement("div");
    card_dom_pd.classList.add("pd");
    card_dom_pd.textcard_dom = +entity.pd;
    card_dom_attributes.appendChild(card_dom_pd);    
    let card_dom_pa = document.createElement("div");
    card_dom_pa.classList.add("pa");
    card_dom_pa.textcard_dom = entity.pa;
    card_dom_attributes.appendChild(card_dom_pa);    
*/
    card_header.appendChild(card_name);
    card_header.appendChild(card_cost);
    card_dom.appendChild(card_header);
    card_dom.appendChild(card_descr);
    hand_content.appendChild(card_dom); 
  };


  if( world.turn == world.player.entity ) {

    hand_dom.classList.add("turn");
  } else {

    hand_dom.classList.remove("turn");
  };
};


function update_world(){

  //TODO resolve dying more locally from entity.location instead of all world

  for ( var i in world.entities ) {

    if ( world.entities[i].pv <= 0 ) {
  
      let dead_name = world.entities[i].name;

      if ( world.entities[i].type != "corpse" ) {
        world.entities[i].art = "./images/entities/corpse.png";
        world.entities[i].name = world.entities[i].name;
        world.entities[i].type = "corpse",
        world.entities[i].pv = 0;
        world.entities[i].hand = [];
        world.entities[i].actions = [];
        world.entities[i].discard = [];
        console.log( dead_name, "died" );
        //break
      };
/*
      for ( var j in world.locations ){
        
        for ( var k = 0 ; k < world.locations[j].content.length ; k++ ){
          
          if ( world.locations[j].content[k].name == dead_name ) {
            
            world.locations[j].content.splice(k,1);
          };
        };
      };

      
      if ( dead_name == world.player.entity.name && world.player.entity.name == dead_name ) {
        
        world.player.entity = undefined;
      };

  */
    };
  };
};


//                                   SETUP
//                                   -----

//                                  Effects
//                                  -------

new Effect( {
  "name" : "blessure",
  "result" : function( target, number ){
    if ( world.turn.status.force ) {
      number += world.turn.status.force;
    };
    let initial_pd = target.pd;
    let initial_number = number;
    let final_number = number - target.pd;
    console.log(number, target.pd);
    if ( final_number < 0 ) final_number = 0;
    target.pd -= initial_number;
    if ( target.pd < 0 ) target.pd = 0;
    target.pv -= final_number;
    return target.name+" blessure : "+initial_number+" - "+initial_pd+" = "
           +final_number+". PV : "+target.pv+" PD : "+target.pd;
  }
} );


new Effect( {
  "name" : "soin",
  "result" : function( target, number ){
    if ( world.turn.status.vitality ) {
      number += world.turn.status.vitality;
    };
    target.pv += number;
    return "PV";
  }
} );

new Effect( {
  "name" : "random",
  "result" : function( target, number ){
    if ( Math.random() * 100 < number ) {
      world.break = true;
      return "Random fail";
    };
  }
} );

new Effect( {
  "name" : "protection",
  "result" : function( target, number ){
    if ( world.turn.status.resistance ) {
      number += world.turn.status.resistance;
    };
    target.pd += number;
    return target.name+" protection = "+number+". PD = "+target.pd;
  }
} );


new Effect( {
  "name" : "temps",
  "result" : function( target, number ){
    target.pa += number;
    return target.name+" temps = "+number+". PA = "+target.pa;
  }
} );


new Effect( {
  "name" : "self",
  "result" : function( target, number ){
    if ( target != world.turn ) {
      world.break = true;
      return "Target must be the entity holding current turn";
    };
  }
} );

new Effect( {
  "name" : "draw",
  "result" : function( target, number ){
    target.draw(number);
  }
} );


new Effect( {
  "name" : "remove",
  "result" : function( target, number ){
    world.turn.discard.pop();
  }
} );


new Effect( {
  "name" : "permanent",
  "result" : function( target, number ){
    target.permanents.push(world.turn.discard.pop());
    if ( target != world.turn ) {
      world.break = true;
      return "Target must be the entity holding current turn";
    };
  }
} );


new Effect( {
  "name" : "force",
  "result" : function( target, number ){
    if ( !target.status["force"] ) {
      target.status["force"] = number;
    } else {
      target.status["force"] += number;
    };
    if ( target != world.turn ) {
      world.break = true;
      return "Target must be the entity holding current turn";
    };
  }
} );


new Effect( {
  "name" : "resistance",
  "result" : function( target, number ){
    if ( !target.status["resistance"] ) {
      target.status["resistance"] = number;
    } else {
      target.status["resistance"] += number;
    };
    if ( target != world.turn ) {
      world.break = true;
      return "Target must be the entity holding current turn";
    };
  }
} );


//                                  Actions
//                                  -------

new Action( {
  "name" : "griffes",
  "type" : "equipement",
  "cost" : 3,
  "effects" : [
    ["self",0],
    ["permanent",0],
    ["random",50],
    ["force",1]
  ],
  "descr":"50% de chances d'ajouter 1 blessure à toutes vos attaques."
} );

new Action( {
  "name" : "épée",
  "type" : "equipement",
  "cost" : 4,
  "effects" : [
    ["self",0],
    ["permanent",0],
    ["force",1]
  ],
  "descr":"Ajoute 1 blessure à toutes vos attaques."
} );

new Action( {
  "name" : "armure",
  "type" : "equipement",
  "cost" : 4,
  "effects" : [
    ["self",0],
    ["permanent",0],
    ["resistance",1]
  ],
  "descr":"Ajoute 1 défense à toutes vos protections."
} );

new Action( {
  "name" : "frappe",
  "type" : "attack",
  "cost" : 1,
  "effects" : [
    ["blessure",1]
  ],
  "descr":"Inflige 1 blessure à une entitée ciblée."
} );

new Action( {
  "name" : "parade",
  "type" : "defense",
  "cost" : 1,
  "effects" : [
    ["self",0],
    ["protection",1]
  ],
  "descr":"Ajoute 1 défense à l'entitée source."
} );

new Action( {
  "name" : "réflexe",
  "type" : "mouvement",
  "cost" : 0,
  "effects" : [
    ["self",0],
    ["temps",2]
  ],
  "descr":"Ajoute 2 points d'action à l'entitée source."
} );

new Action( {
  "name" : "bandage",
  "type" : "objet",
  "cost" : 2,
  "effects" : [
    ["self",0],
    ["soin",4],
    ["remove",0]
  ],
  "descr":"Rends 4 point de vie à l'entitée source."
} );


new Action( {
  "name" : "clairvoyance",
  "type" : "sort",
  "cost" : 1,
  "effects" : [
    ["self",0],
    ["draw",3]
  ],
  "descr":"L'entitée source pioche 3 cartes."
} );


//                                 Locations
//                                 ---------

new Location( {
  "name" : "Prairie",
  "paths" : [ "Forêt" ]
} );


new Location( {
  "name" : "Forêt",
  "paths" : [ "grassland" ]
} );


//                                  Entities
//                                  --------

//                                   Player

var entity;


entity = new Entity( {
  "name" : "Jean l'humain",
  "type" : "humain",
  "art" : "./images/entities/hero.png",
  "pv" : 15,
  "behaviour_need" : 5,
  "discard" : [
    "épée","armure",
    "bandage","bandage",
    "clairvoyance",
    "clairvoyance",
    "frappe","frappe","parade","parade",
    "frappe","frappe","parade","parade",
    "réflexe","réflexe","réflexe","réflexe","réflexe",
    "réflexe","réflexe","réflexe","réflexe","réflexe","réflexe"
  ]
} )


world.player = new Player( { "entity" : entity } );
world.turn = world.player.entity;
entity.move("Forêt");
entity.shuffle();
entity.draw(7);


//                                    PNJ


entity = new Entity( {
  "name" : "Michel le lion",
  "type" : "goblin",
  "art" : "./images/entities/lion.png",
  "pv" : 10,
  "behaviour_need" : 4,
  "discard" : [
    "griffes","frappe","frappe","réflexe","réflexe",
    "frappe","frappe","réflexe","réflexe"
  ]
} );


entity.move("Forêt");
entity.shuffle();
entity.draw(7);









/*
entity = new Entity( {
  "name" : "Raoul le goblin",
  "art" : "./images/goblin_1.png",
  "pv" : 5,
  "discard" : [
    "frappe","frappe","parade","parade","réflexe","réflexe",
    "frappe","frappe","parade","parade","réflexe","réflexe"
  ]
} );


entity.move("Forêt");
entity.shuffle();
entity.draw(7);


entity = new Entity( {
  "name" : "Simon le lion",
  "art" : "./images/lion.png",
  "pv" : 10,
  "discard" : [
    "frappe","frappe","parade","parade","réflexe","réflexe",
    "frappe","frappe","parade","parade","réflexe","réflexe"
  ]
} );


entity.move("Forêt");
entity.shuffle();
entity.draw(7);


entity = new Entity( {
  "name" : "Simone la méduse",
  "art" : "./images/green_medusa.png",
  "pv" : 10,
  "discard" : [
    "frappe","frappe","parade","parade","réflexe","réflexe",
    "frappe","frappe","parade","parade","réflexe","réflexe"
  ]
} );


entity.move("Forêt");
entity.shuffle();
entity.draw(7);


entity = new Entity( {
  "name" : "Bernadette la méduse",
  "art" : "./images/green_medusa.png",
  "pv" : 10,
  "discard" : [
    "frappe","frappe","parade","parade","réflexe","réflexe",
    "frappe","frappe","parade","parade","réflexe","réflexe"
  ]
} );


entity.move("Forêt");
entity.shuffle();
entity.draw(7);


*/
//                                  Console

update_display();

//world.player.entity.end_turn();










/*




function log(string){
  let log = document.createElement("div");
  log.innertext = string;
  document.body.appendChild(log);
};

*/

// NOTES
/*

Alignements / factions

- ordre pour le profit commun (communisme industriel ?)         "Plèbe"
- ordre pour le profit commun et personnel (féodalisme ?)       "Chevalerie"
- ordre pour le profit personnel (autocratie)                   "Empire"
- harmonie pour le profit commun (écologisme ?)                 "Arboroscene"
- harmonie pour le profit commun et personnel (animisme ?)      "Sorcellerie"
- harmonie pour le profit personnel (capitalisme vert ?)        "Purge"
- chaos pour le profit commun (anarchisme ?)                    "Anarchie"
- choas pour le profit commun et personnel (néo-libéralisme ?)  "Négoce"
- chaos pour le profit personnel (ultra-libéralisme ?)          "Élite"

- carte compte à rebours : tombe si tu la détruit pas tu meurt "il invoque ta tombe" faith of destiny( YANNICK )
- goblins : pas de protection mais multiplication ( appeler autre goblin ) 
- carte agilitée : chance d'esquiver une attaque MERCI YANNICK
- carte griffe : chance de gagner une force 3PA
- monstres type "boost" : beaucoup de cartes qui boost les status peu de carte d'attaque ou de défense
- carte vampire : attaque plus récupère un point de vie
- carte poison : malédiction,  si le coût n'est pas payé = malus
- chance comme statut ex barde augmente la chance 
- sommeil paralysie ext.

graphisme : mode inversé
zoom ?
duplication des sprites


*/
